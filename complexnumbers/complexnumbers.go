package complexnumbers

import (
	"fmt"
	i "github.com/skilstak/go-input"
	"strings"
)

// Menu for program
func Menu() {

	menu := i.Ask("Options: Imaginary\n$ ")
	menu = strings.ToLower(menu)
	switch menu {
	case "imaginary":
		imaginary()
	default:
		Menu()
	}
}

func imaginary() {
	fmt.Println("hello there")
}